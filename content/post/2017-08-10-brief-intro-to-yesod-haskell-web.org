---
title: "Brief introduction to Yesod, a Haskell web framework"
date: "2017-08-10"
tags: ["haskell"]
description: "Haskell web programming"
---
** Scaffold
Create a new scaffolded site with the yesod-postgres stack template:
  =stack new project yesod-postgres=

 
=tree ~/projects/project= :
#+BEGIN_SRC 
├── app
│   ├── devel.hs
│   ├── DevelMain.hs
│   └── main.hs
├── config
│   ├── client_session_key.aes
│   ├── favicon.ico
│   ├── keter.yml
│   ├── models
│   ├── robots.txt
│   ├── routes
│   ├── settings.yml
│   ├── test-settings.yml
├── dist
│   └── build
├── Makefile
├── package.yaml
├── src
│   ├── Application.hs
│   ├── Foundation.hs
│   ├── Handler
│   │   ├── Comment.hs
│   │   ├── Common.hs
│   │   ├── Home.hs
│   │   └── Profile.hs
│   ├── Import
│   │   └── NoFoundation.hs
│   ├── Import.hs
│   ├── Model.hs
│   ├── Settings
│   │   └── StaticFiles.hs
│   └── Settings.hs
├── stack.yaml
├── static
│   ├── css
│   │   └── bootstrap.css
│   ├── fonts
│   │   ├── glyphicons-halflings-regular.eot
│   │   ├── glyphicons-halflings-regular.svg
│   │   ├── glyphicons-halflings-regular.ttf
│   │   └── glyphicons-halflings-regular.woff
│   └── tmp
│       ├── autogen-_7G0I2w4.css
│       ├── autogen-8iASS6X3.css
│       ├── autogen-dAT6QVJx.js
│       ├── autogen-dyDfk8nC.css
│       ├── autogen-PE46a7-4.css
│       ├── autogen-r3XaZuvR.js
│       └── autogen-yWbYL5Pf.css
├── templates
│   ├── default-layout.hamlet
│   ├── default-layout.lucius
│   ├── default-layout-wrapper.hamlet
│   ├── default-message-widget.hamlet
│   ├── homepage.hamlet
│   ├── homepage.julius
│   ├── homepage.lucius
│   ├── posts
│   │   ├── new.hamlet
│   └── profile.hamlet
├── test
│   ├── Handler
│   │   ├── CommentSpec.hs
│   │   ├── CommonSpec.hs
│   │   ├── HomeSpec.hs
│   │   └── ProfileSpec.hs
│   ├── seed.hs
│   ├── Spec.hs
│   └── TestImport.hs
├── yesod-devel
│   ├── devel-terminate
│   └── rebuild
└── ysp.cabal

#+END_SRC

The scaffolded site can be compiled and started using =stack exec -- yesod devel= . 
Any changes made will cause the server to be recompiled and the server restarted.

Take a look in the config directory, particularly =config/routes= and =config/models= . 

=config/routes= :
#+BEGIN_SRC 
/static StaticR Static appStatic
/auth   AuthR   Auth   getAuth

/favicon.ico FaviconR GET
/robots.txt RobotsR GET

/ HomeR GET POST
/comments CommentR POST

/profile ProfileR GET

#+END_SRC

** Adding a new route and handler
To add a new route, simply add a line e.g:
#+BEGIN_SRC 
/foo FooR GET
#+END_SRC

This will cause compile to fail because there is no handler called
getFooR. So let's create it. Create a file =src/handlers/Foo.hs= . 
Make it look something like this:

#+BEGIN_SRC haskell
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Foo where
import Import
import Yesod.Form.Bootstrap3 (BootstrapFormLayout (..), renderBootstrap3)

getAddFeedR :: Handler Html
getAddFeedR = do
    defaultLayout $ do
        setTitle "Foo handler"
        $(widgetFile "foo")
#+END_SRC

Note how we reference a file =foo= with the =widgetFile= template
haskell function, that is referencing =templates/foo.hamlet=. Let's create it:

#+BEGIN_SRC hamlet
<div .container>
    foo!
#+END_SRC

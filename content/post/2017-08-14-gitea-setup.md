--- 
title: "How to use your own Github replacement with Gitea" 
date: "2017-08-14" 
--- 
Install Gitea on your server, configure `app.ini` with
a custom SSH port and start the built in SSH server (
`START_SSH_SERVER = true` ).
<https://gogs.io/docs/advanced/configuration_cheat_sheet>

On your client, edit `~/.ssh/config`

    Host dev
        HostName dev.example.com
        Port 22000
        User fooey

<http://nerderati.com/2011/03/17/simplify-your-life-with-an-ssh-config-file/>

You can now use git remotes thus:
`git remote add origin dev:/fooey/myproject.git`

Entered on \[2017-08-14 mån 15:37\]
